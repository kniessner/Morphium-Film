$(document).ready(function () {

  $('#main_back_btn').click(function (event) {
    $('body').addClass("demoreel_activ");
  });
  $('#demoreel_jumper').click(function (event) {
    $('body').removeClass("demoreel_activ");
  });
  //setIframeHeight(document.getElementById("#demoreel_video"));

  $('#prime_slider .carousel-item').click(function (event) {
    console.log('clicked ' + event.delegateTarget);
    var dataSrc = $(event.delegateTarget).find('#demoreel_video').attr("data-src");

    if (dataSrc) {
      $("#demoreel_video").attr("src", dataSrc);
      $('#demoreel_video').on('load', function () {
        $('#demoreel_video').parent().removeClass("unloaded");

      });
    } else {
      $("#demoreel_video").attr("src", "");
      $('#demoreel_video').parent().addClass("unloaded");

    }

  });



  $("img[data-src]").each(function () {
    var sourceFile = $(this).data('src');
    $(this).attr('src', sourceFile);
    $(this).ready(function () {
      $(this).removeAttr('data-src');
    });
  });

  $("video#starter_video source[data-src]").each(function () {
    var sourceFile = $(this).data('src');
    console.log('lazy_loaded video');
    $(this).attr('src', sourceFile);
    var video = this.parentElement;
    video.load();
    video.play();
    $(this).ready(function () {
      $(this).removeAttr('data-src');
    });
  });






  /*
    var vid = $("video#starter_video");
    var pauseButton = $("#prime_slider button.paus_play");

    if (window.matchMedia('(prefers-reduced-motion)').matches) {
      vid.removeAttribute("autoplay");
      vid.pause();
      pauseButton.innerHTML = "Paused";
    } */

  /*  function vidFade() {
   //  vid.classList.add("stopfade");
   } */

  /*   vid.addEventListener('ended', function () {
      // only functional if "loop" is removed
      vid.pause();
      // to capture IE10
      vidFade();
    });
   */
  /*   pauseButton.on('click', function () {
      vid.toggleClass("stopfade");
      if (vid.paused) {
        $("video#starter_video").play();
        pauseButton.innerHTML = "Pause";
      } else {
        // $("video#starter_video").pause();
        pauseButton.innerHTML = "Paused";
      }
    }) */

});


jQuery(document).ready(function ($) {
  //$("#overlay_svg").delay(4500).fadeOut(2500).addClass('loaded');
});
