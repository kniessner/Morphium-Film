  jQuery(document).ready(function ($) {

    send_post_req(1,'');

    $('#video_post_filter .category').click(function (event) {
      event.preventDefault();
      $("#video_post_filter .selected").removeClass('selected');
      $(this).addClass('selected');
      var cat = $(this).data('cat');
      $('#video_grid').data('cat');
      send_post_req(1, cat);
    });

    $('#next_projectpage').click(function (event) {
      event.preventDefault();
      var current = $('#video_grid').data('current');
       var next = current + 1;
      console.log("current page " + current, "next "+next);
      var cat = $("#video_post_filter .selected").first().data('cat') ? $("#video_post_filter .selected").first().data('cat') : '';

      send_post_req(next, cat);
    });

    $('#prev_projectpage').click(function (event) {
      event.preventDefault();
      var current = $('#video_grid').data('current');
      var next = current - 1;
      console.log("current page " + current, "next "+next);

      var cat = $("#video_post_filter .selected").first().data('cat') ? $("#video_post_filter  .selected").first().data('cat') : '';

      send_post_req(next, cat);
    });

    function send_post_req(page, cat) {


        $.ajax({
        url: ajaxpagination.ajaxurl,
        type: 'post',
        data: {
          action: 'ajax_pagination',
          category_name: cat,
          page: page
        },
        success: function (data, textStatus, XMLHttpRequest) {
          var data = JSON.parse(data);
          $("#loadvideos").html('');
          $("#video_grid ").attr('data-total', data.num_pages);
          $("#video_grid ").data('current', page);

       //   $("#prev_projectpage").hide();
        //  if (page > 1)
        //  $("#next_projectpage").show();
         // console.log(page, cat, data.num_pages)

          if(page < data.num_pages){
             $("#next_projectpage").show();
          }else{
             $("#next_projectpage").hide();
          }

          if(page > 1) {
             $("#prev_projectpage").show();
          } else {
             $("#prev_projectpage").hide();
          }
         // console.log("ajax req received: ", data);
          $.each(data.result, function (key, value) {

            var html = value['html'];
            $("#loadvideos").delay(500).append(html);

            $("#loadvideos .grid-item").hide().last().delay(500).fadeIn(key * 500).addClass('loaded').click(function (event) {
              console.log("video tile clicked", event);

            });


            $("#loadvideos img[data-src]").each(function () {
              var sourceFile = $(this).data('src');
              $(this).attr('src', sourceFile);
              $(this).ready(function () {
                $(this).removeAttr('data-src');
                var w = window.innerWidth;

              });
            });


          });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          alert(errorThrown);
        }
      });
    }
  });
