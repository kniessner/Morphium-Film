<?php
function acf_set_featured_image($image_id, $post_id, $field){
    echo $field;
    echo $image_id;
    if ($image_id != '') {
        add_post_meta($post_id, '_thumbnail_id', $image_id);
    }

    return $image_id;
}
add_filter('acf/update_value/name=main_image', 'acf_set_featured_image', 10, 3);
add_action('save_post', 'set_featured_image_from_gallery');
function set_featured_image_from_gallery()
{
        $images = get_field('images', false, false);
        $image_id = $images[0];
    if ($image_id) {
        set_post_thumbnail($post->ID, $image_id);
    }
}
?>