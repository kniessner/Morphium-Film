<?php


/********************************************************************************
Custom Post-types
 ********************************************************************************/

add_action('init', 'custom_post_type');
// Register Custom Post Type
function custom_post_type()
{

	$labels = array(
		'name' => _x('Kunden', 'Post Type General Name', 'mrph_domain'),
		'singular_name' => _x('Kunden', 'Post Type Singular Name', 'mrph_domain'),
		'menu_name' => __('Kunden', 'mrph_domain'),
		'name_admin_bar' => __('Kunden', 'mrph_domain'),
		'archives' => __('Kunden Archiv', 'mrph_domain'),
		'attributes' => __('Kunden Attribute', 'mrph_domain'),
		'parent_item_colon' => __('Parent Item:', 'mrph_domain'),
		'all_items' => __('Alle Kunden', 'mrph_domain'),
		'add_new_item' => __('Neuen Kunde hinzufügen', 'mrph_domain'),
		'add_new' => __('Kunde hinzufügen', 'mrph_domain'),
		'new_item' => __('Neuer Kunde', 'mrph_domain'),
		'edit_item' => __('Kunde bearbeiten', 'mrph_domain'),
		'update_item' => __('Update Kunde', 'mrph_domain'),
		'view_item' => __('Kunde ansehen', 'mrph_domain'),
		'view_items' => __('Kunden ansehen', 'mrph_domain'),
		'search_items' => __('Kunde suchem', 'mrph_domain'),
		'not_found' => __('Not found', 'mrph_domain'),
		'not_found_in_trash' => __('Nicht gefunden in Trash', 'mrph_domain'),
		'featured_image' => __('Featured Image', 'mrph_domain'),
		'set_featured_image' => __('Set featured image', 'mrph_domain'),
		'remove_featured_image' => __('Remove featured image', 'mrph_domain'),
		'use_featured_image' => __('Use as featured image', 'mrph_domain'),
		'insert_into_item' => __('Insert into item', 'mrph_domain'),
		'uploaded_to_this_item' => __('Uploaded to this item', 'mrph_domain'),
		'items_list' => __('Items list', 'mrph_domain'),
		'items_list_navigation' => __('Items list navigation', 'mrph_domain'),
		'filter_items_list' => __('Filter items list', 'mrph_domain'),
	);
	$args = array(
		'label' => __('Kunden', 'mrph_domain'),
		'description' => __('Kundenbeschreibung', 'mrph_domain'),
		'labels' => $labels,
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-universal-access',
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'capability_type' => 'page',
	);
	register_post_type('Kunden', $args);


	$labels = array(
		'name' => _x('Projekte', 'Post Type General Name', 'mrph_domain'),
		'singular_name' => _x('Projekte', 'Post Type Singular Name', 'mrph_domain'),
		'menu_name' => __('Projekte', 'mrph_domain'),
		'name_admin_bar' => __('Projekte', 'mrph_domain'),
		'archives' => __('Projekt Archives', 'mrph_domain'),
		'attributes' => __('Projekt Attributes', 'mrph_domain'),
		'parent_item_colon' => __('Parent Projekt:', 'mrph_domain'),
		'all_items' => __('Alle Projekte', 'mrph_domain'),
		'add_new_item' => __('Neues Projekt hinzufügen', 'mrph_domain'),
		'add_new' => __('Neues Projekt', 'mrph_domain'),
		'new_item' => __('Neues Projekt', 'mrph_domain'),
		'edit_item' => __('Edit Projekt', 'mrph_domain'),
		'update_item' => __('Update Projekt', 'mrph_domain'),
		'view_item' => __('View Projekt', 'mrph_domain'),
		'view_items' => __('View Projekte', 'mrph_domain'),
		'search_items' => __('Projekt suchen', 'mrph_domain'),
		'not_found' => __('Projekt nicht gefunden', 'mrph_domain'),
		'not_found_in_trash' => __('Nicht gefunden in Trash', 'mrph_domain'),
		'featured_image' => __('Featured Image', 'mrph_domain'),
		'set_featured_image' => __('Set featured image', 'mrph_domain'),
		'remove_featured_image' => __('Remove featured image', 'mrph_domain'),
		'use_featured_image' => __('Use as featured image', 'mrph_domain'),
		'insert_into_item' => __('Insert into Projekt', 'mrph_domain'),
		'uploaded_to_this_item' => __('Uploaded to this Projekt', 'mrph_domain'),
		'items_list' => __('Projekte list', 'mrph_domain'),
		'items_list_navigation' => __('Projekte list navigation', 'mrph_domain'),
		'filter_items_list' => __('Filter Projekte list', 'mrph_domain'),
	);
	$args = array(
		'label' => __('Projekte', 'mrph_domain'),
		'description' => __('Projektbeschreibung', 'mrph_domain'),
		'labels' => $labels,
		'taxonomies' => array('category', 'post_tag'),
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-media-video',
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'capability_type' => 'page',
	);
	register_post_type('Projekte', $args);

	$labels = array(
		'name' => _x('Team', 'Post Type General Name', 'mrph_domain'),
		'singular_name' => _x('Team', 'Post Type Singular Name', 'mrph_domain'),
		'menu_name' => __('Team', 'mrph_domain'),
		'name_admin_bar' => __('Team', 'mrph_domain'),
		'archives' => __('Item Archives', 'mrph_domain'),
		'attributes' => __('Item Attributes', 'mrph_domain'),
		'parent_item_colon' => __('Parent Member:', 'mrph_domain'),
		'all_items' => __('Alle Kollegen', 'mrph_domain'),
		'add_new_item' => __('Neuer Kollege', 'mrph_domain'),
		'add_new' => __('Kollege hinzufügen', 'mrph_domain'),
		'new_item' => __('Neuer Kollege', 'mrph_domain'),
		'edit_item' => __('Kollege bearbeiten', 'mrph_domain'),
		'update_item' => __('Update Member', 'mrph_domain'),
		'view_item' => __('View Kollege', 'mrph_domain'),
		'view_items' => __('View Kollegen', 'mrph_domain'),
		'search_items' => __('Kollege suchen', 'mrph_domain'),
		'not_found' => __('Not found', 'mrph_domain'),
		'not_found_in_trash' => __('Nicht gefunden in Trash', 'mrph_domain'),
		'featured_image' => __('Featured Image', 'mrph_domain'),
		'set_featured_image' => __('Set featured image', 'mrph_domain'),
		'remove_featured_image' => __('Remove featured image', 'mrph_domain'),
		'use_featured_image' => __('Use as featured image', 'mrph_domain'),
		'insert_into_item' => __('Insert into item', 'mrph_domain'),
		'uploaded_to_this_item' => __('Uploaded to this item', 'mrph_domain'),
		'items_list' => __('Items list', 'mrph_domain'),
		'items_list_navigation' => __('Items list navigation', 'mrph_domain'),
		'filter_items_list' => __('Filter items list', 'mrph_domain'),
	);
	$args = array(
		'label' => __('Team', 'mrph_domain'),
		'description' => __('Post Type Description', 'mrph_domain'),
		'labels' => $labels,
		'supports' => array('title', 'editor', 'thumbnail'),
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-groups',
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'capability_type' => 'page',
	);
	register_post_type('Team', $args);
}

add_action('init', 'custom_post_type', 0);
?>
