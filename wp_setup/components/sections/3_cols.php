
    <div class="section_content container">
        <div class="col col-md-4">

            <?php the_sub_field('column_1'); ?>

        </div>
        <div class="col col-md-4">

            <?php the_sub_field('column_2'); ?>

        </div>
         <div class="col col-md-4">

            <?php the_sub_field('column_3'); ?>

        </div>
    </div>
