<!--Button trigger modal
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Contact_Modal" data-whatever="@mdo">
  Launch demo modal
</button>
 -->
<!-- Modal -->
<div class="modal fade" id="Contact_Modal" tabindex="-1" role="dialog" aria-labelledby="Contact_Modal_Title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="Contact_Modal_Title">Kontaktformular</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <?php 
            //$contact_page = get_page_by_title('Kontakt');
            $contact_form = get_field('contact_form', 'option');
    
            echo do_shortcode("" . $contact_form);

          
           ?>
      </div>
   <!--    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>