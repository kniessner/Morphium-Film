<?php add_action('wp_ajax_nopriv_ajax_pagination', 'ajax_pagination');
add_action('wp_ajax_ajax_pagination', 'ajax_pagination');



function ajax_pagination() {
    $paged = $_POST['page'];
    $category_name=$_POST['category_name'];
    $cards=array();
    $args=array(
      'post_type'=> array('projekte'),
      'post_status'=> array('publish'),
      'category_name'=> $category_name,
      'paged'=> $paged, 'posts_per_page'=> 9,
      'meta_query'=> array( array( 'key'=> 'videos', 'compare'=> 'EXISTS'),
                array(
                    'key' => 'featured',
                     'compare' => 'EXISTS'
                ),
    ));
    $loop=new WP_Query($args);
    $num_pages=$loop->max_num_pages;
    $num_posts=$loop->post_count;
    $counter=0;


    if ($loop->have_posts()): while ($loop->have_posts()): $loop->the_post();
    $counter++;
    $videos=get_field('videos');
    $client = get_field('client');
    $first_video=$videos[0]['video_id'];
    $card='';
    $post_categories = get_the_terms(get_the_ID(), 'category' );
    if ( ! empty( $post_categories ) && ! is_wp_error( $post_categories ) ) {
        $categories = wp_list_pluck( $post_categories, 'name' );
    }

    $video_img="https://img.youtube.com/vi/" . $first_video . "/hqdefault.jpg";
    $video_url="https://www.youtube.com/embed/" . $first_video . "?autoplay=1&autohide=1&yt:stretch=16:9&showinfo=0&rel=0&modestbranding=1";
    $card=$card . '<div class="col col-12 col-md-6  col-lg-4"><figure class="project_card open_modal"
    data-number="'.$counter.'"
    data-id="'.get_the_ID(). '"
    data-link="'.get_permalink().'"
    data-cats='.json_encode($categories).'
    data-client="' .get_the_title( $client->ID ) . '"
    data-clientimage= "'.get_the_post_thumbnail_url( $client->ID , 'medium').'"
    data-production="' . get_sub_field('production') . '"
    data-video="' . $video_url . '"
    data-image="' . $video_img . '" data-toggle="modal"
    data-urltitle="'.urlencode(get_the_title()).'"
    data-target="#mainModal">';
    $card=$card . '<figcaption>';
    $card=$card . '<div class="info"><h3 class="inner_tile">'. get_the_title( ).'</h3>';
    $card=$card . '<h4 class="sub_title">' . get_sub_field('subtitle').'</h4>';
   if($client->ID) $card=$card . '<p class="client">'.  get_the_title($client->ID) .'</p>';

    $card=$card . '<p class="description">'. get_the_content() .'</p>';

    $card=$card . '</div></figcaption>';
    $card=$card . '<img class="card_image" data-src="' . $video_img . '" />';


    $card=$card . '</figure></div>';
    array_push($cards, array('html'=> $card));
    endwhile;
    wp_reset_query();
    endif;
    echo json_encode(array('success'=> true, 'num_pages'=> $num_pages, 'result'=> $cards));
    exit();
}

?>
