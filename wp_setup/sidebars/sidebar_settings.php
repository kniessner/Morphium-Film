<?php

/* Register sidebars. */
add_action( 'widgets_init', 'complex_register_sidebars', 5 );
function complex_register_sidebars() {

	hybrid_register_sidebar(
		array(
			'id'          => 'posts',
			'name'        => _x( 'Posts', 'sidebar', 'k_complex' ),
			'description' => __( 'Shown on posts and blog index.', 'k_complex' )
		)
	);
    hybrid_register_sidebar(
        array(
            'id'          => 'default',
            'name'        => _x( 'Default', 'sidebar', 'k_complex' ),
            'description' => __( 'The default global sidebar', 'k_complex' )
        )
    );

}

?>